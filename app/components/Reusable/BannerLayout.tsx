export default function BannerLayout({
  banner,
  bannerTitle,
  bannerInfo,
}: {
  banner: string,
  bannerTitle: string,
  bannerInfo: string,
}) {
  return (
    <div className="grid mt-5 tab:mt-6 text-center">
      <img src={banner} className="" />
      <div className="absolute w-full">
        <p className="Dosis-Bold text-white text-6xl text-center mt-32">{bannerTitle}</p>
        <p className="Dosis-Regular text-3xl text-white max-w-lg mx-auto mt-10">
          {bannerInfo}
        </p>
      </div>
    </div>
  )
}
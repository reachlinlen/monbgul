import { Form, useActionData, useTransition } from "remix";

export function CareerForm() {
  const transition = useTransition();
  const actionData = useActionData();

  return (
    <div className="py-6 pl-4 mr-2 text-xs bg-white shadow-md text-contact tab:text-2xl h-60 tab:h-full rounded-3xl Dosis-SemiBold">
      <Form method="post" className="w-11/12">
        <fieldset disabled={transition.state === "submitting"}>
          <p>
            <label className="text-xs text-black tab:text-base Dosis-Regular">Name
              <input
                type="text"
                name="name"
                required
                defaultValue={actionData ? actionData.Values.name : undefined}
                className="mt-4 ml-4"
              />
            </label>
          </p>
          <hr />
          <p>
            <label className="text-xs text-black tab:text-base Dosis-Regular">Email
              <input
                type="email"
                name="email"
                required
                defaultValue={actionData ? actionData.Values.email : undefined}
                className="mt-4 ml-4"
              />
            </label>
          </p>
          <hr />
          <p>
            <label className="text-xs text-black tab:text-base Dosis-Regular">Mobile Number
              <input
                type="tel"
                name="mobile"
                required
                defaultValue={actionData ? actionData.Values.mobile : undefined}
                className="mt-4 ml-4"
              />
            </label>
          </p>
          <hr />
          <p>
            <label className="text-xs text-black tab:text-base Dosis-Regular">Position
              <input
                type="text"
                name="position"
                required
                defaultValue={actionData ? actionData.Values.position : undefined}
                className="mt-4 ml-4"
              />
            </label>
          </p>
          <hr />
          <button className="right-0 px-2 py-1 mt-4 text-xs text-white bg-title tab:px-6 tab:text-base rounded-3xl">
            Upload Resume
          </button>
        </fieldset>
      </Form>
    </div>
  )
}
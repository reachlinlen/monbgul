import { SpecialityDetail } from "./Typing";

export default function BeltDisplay({
  specialityDetails
}: {
  specialityDetails: SpecialityDetail[]
}) {
  return (
    <div className="">
      <div className="h-32 px-16 bg-gradient-dark-light rounded-3xl">
        <div className="flex justify-between text-white h-full items-center">
          <div className="text-center">
            <p className="Dosis-Bold text-7xl">{specialityDetails[0].count}+</p>
            <p className="Dosis-Regular text-xl">{specialityDetails[0].type}</p>
          </div>
          <div className="text-center">
            <p className="Dosis-Bold text-7xl">{specialityDetails[1].count}+</p>
            <p className="Dosis-Regular text-xl">{specialityDetails[1].type}</p>
          </div>
          <div className="text-center">
            <p className="Dosis-Bold text-7xl">{specialityDetails[2].count}+</p>
            <p className="Dosis-Regular text-xl">{specialityDetails[2].type}</p>
          </div>
        </div>
      </div>
      <div className="h-10 w-[calc(100%*0.02)] bg-title rounded-l-2xl -ml-4 -mt-20" />
      <div className="h-10 w-[calc(100%*0.02)] bg-lightRedGradientEnd rounded-l-2xl -mr-4 -mt-10 float-right -rotate-180" />
    </div>
  )
}
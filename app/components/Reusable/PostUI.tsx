import { Link } from "remix";

export default function PostUI({
  topic,
  description,
  URL,
  svg,
  numberPostsPerLine,
  bgWhite
}: {
  topic: string,
  description: string,
  URL: string,
  svg: string,
  numberPostsPerLine: number,
  bgWhite?: boolean
}) {

  function ThreePostsInALine() {
    return (
      <div className={`group w-72 h-48 drop-shadow-lg odd:bg-white even:bg-bgGrey rounded-xl 
            hover:bg-gradient-light-dark`}>
        <Link to={URL}>
          <div className="grid grid-cols-6 text-left p-3 pt-6 group-hover:text-white">
            <div className="col-start-1 col-end-6 h-20">
              <p className="Dosis-SemiBold text-2xl">{topic}</p>
            </div>
            <div className="col-start-1 col-end-5">
              <p className="mt-2 text-title Dosis-SemiBold text-lg group-hover:text-white">Read More...</p>
            </div>
            <div className="col-start-5 col-end-7">
              <img src={svg} className="" />
            </div>
          </div>
        </Link>
      </div>
    )
  }

  function TwoPostsInALine() {
    return (
      <div className={`group w-420 h-72 text-left drop-shadow-lg m-2 rounded-xl 
            ${bgWhite ? 'bg-white' : 'bg-bgGrey'} hover:bg-gradient-light-dark`}
      >
        <Link to={URL}>
          {/* <div className="grid grid-cols-3 grid-rows-2">
            <div className="col-start-1 col-end-3 row-start-1 row-end-2 pl-8">
              <p className="Dosis-SemiBold text-2xl pt-8">Building Insuvaii</p>
              <p>Best practices always bring best products & solutions within the stipulated time.</p>
            </div>
            <div className="col-start-1 col-end-2 row-start-2 row-end-3">
              <p className="text-title Dosis-Medium text-lg pl-8">Read More...</p>
            </div>
            <div className="col-start-2 col-end-4 row-start-2 row-end-3 h-36">
              <img src={svg} className="h-72" />
            </div>
          </div> */}
          <div className="w-full h-full pl-8 pt-8 group-hover:text-white">
            <p className="w-10/12 Dosis-Bold text-2xl">{topic}</p>
            <p className="w-3/4 pt-4 Dosis-Medium">{description}</p>
            <p className="text-title Dosis-SemiBold text-lg pt-4 group-hover:text-white">Read More...</p>
          </div>
        </Link>
      </div>
    )
  }

  return (
    numberPostsPerLine == 2 ?
      <TwoPostsInALine />
      :
      <ThreePostsInALine />
  )
}
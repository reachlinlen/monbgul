import Paper from '@mui/material/Paper';

export function TagsDropdown({
  tags
}: {
  tags?: string[]
}) {
  return (
    <Paper elevation={3}>
      <div className="flex flex-wrap justify-center gap-4">
        {
          tags && tags.map(tag => tag)
        }
      </div>
    </Paper>
  )
}
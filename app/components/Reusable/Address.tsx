import Location from "../../svg/location.svg";
import Mail from "../../svg/mail.svg";
import Mobile from "../../svg/mobile.svg";

export function Address() {
  return (
    <address className="grid grid-cols-12 gap-y-1">
      <div className="col-start-1 col-end-2 justify-self-center">
        <img src={Location} className="w-6" />
      </div>
      <div className="col-start-2 col-end-13">
        <p>#12/48, Fernvale, Singapore - 791409</p>
      </div>
      <div className="col-start-1 col-end-2 justify-self-center">
        <img src={Mail} className="w-6" />
      </div>
      <div className="col-start-2 col-end-13">
        <p>monbgul@monbgul.com</p>
      </div>
      <div className="col-start-1 col-end-2 justify-self-center">
        <img src={Mobile} className="w-4" />
      </div>
      <div className="col-start-2 col-end-13">
        <p>+65 91090503</p>
      </div>
    </address>
  )
}
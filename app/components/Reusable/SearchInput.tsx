import Search from "../../svg/searchIcon.svg";

export function SearchInput({
  placeholderText,
  callFocus
}: {
  placeholderText: string,
  callFocus?: () => void;
}) {
  return (
    <div className="grid">
      <input
        type="text"
        id="search"
        placeholder={placeholderText}
        onFocus={callFocus}
        onBlur={callFocus}
        className="w-full h-6 pl-11 bg-bgGrey rounded-xl Dosis-Regular tab:h-12"
      />
      {/* <datalist id="tag-list" className="w-full">
        {
          tagList.map(list => (<option value={list} />))
        }
      </datalist> */}
      <img src={Search} className="w-6 ml-3 mt-3 absolute" />
    </div>
  )
}
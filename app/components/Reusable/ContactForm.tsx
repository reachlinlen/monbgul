import { Form, useActionData, useTransition } from "remix";

const CONTACT_ROW_NUMBERS = 8;
export function ContactForm() {
  const transition = useTransition();
  const actionData = useActionData();

  return (
    <div className="p-4 text-xl bg-white shadow-md tab:text-2xl h-60 tab:h-fit rounded-3xl Dosis-SemiBold text-contact">
      <Form method="post">
        <fieldset disabled={transition.state === "submitting"}>
          <p>
            <label className="text-xs text-black tab:text-base Dosis-Regular">Name
              <input
                type="text"
                name="name"
                required
                defaultValue={actionData ? actionData.Values.name : undefined}
                className="mt-4 ml-4"
              />
            </label>
          </p>
          <hr />
          <p>
            <label className="text-xs text-black tab:text-base Dosis-Regular">Email
              <input
                type="email"
                name="email"
                required
                defaultValue={actionData ? actionData.Values.email : undefined}
                className="mt-4 ml-4"
              />
            </label>
          </p>
          <hr />
          <p>
            <label className="text-xs text-black tab:text-base Dosis-Regular">Company
              <input
                type="text"
                name="company"
                required
                defaultValue={actionData ? actionData.Values.company : undefined}
                className="mt-4 ml-4"
              />
            </label>
          </p>
          <hr />
          <p>
            <label className="text-xs text-black tab:text-base Dosis-Regular">Enquiry
              <textarea
                rows={CONTACT_ROW_NUMBERS}
                name="enquiry"
                required
                defaultValue={actionData ? actionData.Values.enquiry : undefined}
                className="mt-4 w-4/5"
              />
            </label>
          </p>
          <hr />
          <button className="px-2 py-1 mt-4 text-xs text-white bg-title tab:px-6 tab:text-base rounded-3xl">
            Send
          </button>
        </fieldset>
      </Form>
    </div>
  )
}
import Instagram from "../../svg/instagram.svg";
import LinkedIn from "../../svg/linkedin.svg";
import Facebook from "../../svg/facebook.svg";

export function SocialMedia() {
  return (
    <div className="grid gap-y-4 tab:flex tab:flex-row tab:justify-center gap-x-4 tab:gap-y-0">
      <img src={Instagram} className="w-5 tab:w-8" />
      <img src={LinkedIn} className="w-5 tab:w-8" />
      <img src={Facebook} className="w-5 tab:w-8" />
    </div>
  )
}
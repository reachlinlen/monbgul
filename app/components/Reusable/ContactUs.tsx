import { Link } from "remix";
import { Address } from "./Address";
import { SocialMedia } from "./SocialMedia";
import { ContactForm } from "./ContactForm";
import Logo from "../../svg/logo.svg";

export function Contact() {
  return (
    <section id="contact" className="bg-contact bg-no-repeat bg-full-nwk-bg">
      <div className="pt-12 pb-6 largeMob:max-w-980 max-w-360 mx-auto">
        {/* for mob */}
        <div className="grid grid-cols-12 tab:hidden gap-y-4">
          <div className="col-start-1 col-end-11 text-base Dosis-SemiBold">
            <Address />
          </div>
          <div className="col-start-11 col-end-13 justify-self-center">
            <SocialMedia />
          </div>
          <div className="col-start-2 col-end-12">
            <ContactForm />
            <div className="flex flex-row justify-center gap-x-4">
              <p className="mt-8 text-black">copywrites</p>
              <p className="mt-8 text-black">Privacy policy</p>
            </div>
          </div>
        </div>
        {/* for tab */}
        <div className="grid grid-cols-12 grid-rows-3 text-bgGrey Dosis-Regular">
          <div className="col-start-1 col-end-3 row-start-1 row-end-3">
            <Link to="/">
              <h3>HOME</h3>
            </Link>
            <ul className="my-2">
              <a href="/#about">
                <li>About Us</li>
              </a>
              <a href="/#services">
                <li>Services</li>
              </a>
              <a href="/#whyus">
                <li>Why Us</li>
              </a>
              <a href="/#whyus">
                <li>Skill Set</li>
              </a>
              <a href="/#partners">
                <li>Partners</li>
              </a>
              <a href="/#team">
                <li>Our Team</li>
              </a>
              <a href="/#careers">
                <li>Careers</li>
              </a>
            </ul>
          </div>
          <div className="col-start-3 col-end-5 row-start-1 row-end-3">
            <h3>THIRANGAL</h3>
            <ul className="my-2">
              <Link to="/thirangal/experience">
                <li>Experience</li>
              </Link>
              <Link to="/thirangal/expertise">
                <li>Expertise</li>
              </Link>
              <Link to="/thirangal/experiment">
                <li>Experiment</li>
              </Link>
            </ul>
          </div>
          <div className="col-start-9 col-end-13 row-start-1 row-end-4">
            <h3>CONNECT WITH US</h3>
            <div className="my-4">
              <SocialMedia />
            </div>
            <ContactForm />
          </div>
          <div className="col-start-1 col-end-6 row-start-3 row-end-4">
            <img src={Logo} className="w-1/3 pb-2" />
            <div className="-ml-1">
              <Address />
            </div>
          </div>
        </div>
        {/* <div className="hidden tab:grid tab:grid-cols-2">
          <div className="col-start-1 col-end-2 text-2xl Dosis-SemiBold">
            <Address />
            <div className="ml-8">
              <div className="flex flex-row mt-16 gap-x-4">
                <SocialMedia />
              </div>
              <p className="mt-8 text-black">copywrites</p>
              <p className="mt-8 text-black">Privacy policy</p>
            </div>
          </div>
          <div className="col-start-2 col-end-3">
            <div className="w-11/12">
              <ContactForm />
            </div>
          </div>
        </div> */}
      </div>
      <footer>
        <div className="text-center text-bgGrey Dosis-Regular py-2">
          <p>Copyright © 2022 Monbgul Technologies. All rights reserved.</p>
        </div>
      </footer>
    </section>
  )
}
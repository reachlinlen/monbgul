import * as React from "react";
import PostUI from "../Reusable/PostUI";
import { SearchInput } from "../Reusable/SearchInput";
import CloudIllustration from '~/svg/nativeCloudIllustration.svg';
import BeltDisplay from "../Reusable/BeltDisplay";
import { SpecialityDetail } from "../Reusable/Typing";
import { TagsDropdown } from "../Reusable/TagsDropdown";
import { useMdxComponent } from "~/utils/mdx";

const BG_WHITE = [0, 3, 4, 7, 8, 11, 12, 15, 16, 19, 20];

export type Post = {
  topic: string,
  description: string,
  URL: string
}

export type Post2 = {
  code: any,
  frontmatter: any,
}

export function ThirangalLayout({
  postsList,
  numberPostsPerLine,
  placeholderText,
  specialityDetails,
  tags
}: {
  postsList: Post2[],
  numberPostsPerLine: number,
  placeholderText: string,
  specialityDetails?: SpecialityDetail[],
  tags?: string[]
}) {
  const [showTags, setShowTags] = React.useState(false);
  function callFocus() {
    if (showTags) {
      setShowTags(false)
    } else {
      setShowTags(true)
    }
  }
  console.log({ postsList })
  const { code, frontmatter } = postsList[0]
  const Component = useMdxComponent(code)
  // return (
  //   <>
  //     <Component />
  //   </>
  // )
  return (
    <Component />
    // <main className="bg-no-repeat bg-full-nwk-bg bg-right mb-8">
    //   <div className="my-2 w-1/2 mx-auto">
    //     <SearchInput placeholderText={placeholderText} callFocus={callFocus} />
    //   </div>
    //   {
    //     showTags && <TagsDropdown tags={tags} />
    //   }
    //   {
    //     specialityDetails &&
    //     <div className="w-3/4 h-32 mx-auto my-4">
    //       <BeltDisplay specialityDetails={specialityDetails} />
    //     </div>
    //   }
    //   <div className="flex flex-wrap justify-center gap-x-2 gap-y-2 -mx-2 mt-2">
    //     {
    //       postsList.map((post, index) =>
    //         <PostUI
    //           topic={post.topic}
    //           description={post.description}
    //           URL={post.URL}
    //           svg={CloudIllustration}
    //           numberPostsPerLine={numberPostsPerLine}
    //           bgWhite={BG_WHITE.includes(index)}
    //           key={post.topic}
    //         />
    //       )
    //     }
    //   </div>
    // </main >
  )
}
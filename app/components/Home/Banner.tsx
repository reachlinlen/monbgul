import { Link } from 'remix'
import CloudIllustration from '../../svg/cloudIllustration.svg'

export function Banner() {
  return (
    <div className="bg-no-repeat bg-banner-pattern p-1 mt-5 tab:p-2 tab:mt-6">
      <div className="grid grid-cols-9 pb-1 tab:pb-4 tab:py-2">
        <div className="col-start-1 col-end-5">
          <img src={CloudIllustration} className="pt-2 pl-4 tab:pl-10" />
        </div>
        <div className="z-10 col-start-5 col-end-10 pt-4 pr-4 text-xs text-right text-white largeMob:pt-8 tab:pt-20 tab:pr-12 tab:text-2xl laptop:text-4xl Dosis-Bold">
          <div className="flex flex-col">
            <div>
              <p>Empowering SME's by adopting</p>
              <p className="text-2xl tab:text-4xl laptop:text-6xl">Cloud services</p>
              <p>to run applications effectively.</p>
            </div>
            <div className="pt-6 tab:pt-12">
              <Link to="#contact">
                <button className="px-2 py-1 mt-auto text-xs bg-white tab:px-6 tab:py-3 tab:text-2xl text-boxSecond rounded-3xl Dosis-Bold">
                  Contact Us
                </button>
              </Link>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}
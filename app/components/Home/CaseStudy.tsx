import Arrow from "../../svg/arrow.svg";

export function CaseStudy() {
  return (
    <section id="casestudy" className="relative z-10">
      <h1 className="hidden my-6 tab:block">CASE STUDIES</h1>
      <h5 className="block my-3 tab:hidden">CASE STUDIES</h5>
      <div className="flex px-2">
        <img
          src={Arrow}
          alt="Click forward to get old set of three case studies"
          className="w-3 tab:w-5"
        />
        {/* for mobile */}
        <div className="tab:hidden">
          <div className="mx-2 bg-no-repeat bg-title rounded-3xl">
            <div className="grid grid-cols-12 p-4 text-white">
              <div className="col-start-1 col-end-10">
                <p className="text-2xl Dosis-Medium">BUILDING INSUVAII</p>
                <p className="text-xs Dosis-Regular">Brief two line description to create interest similar to headline!</p>
              </div>
              <div className="col-start-10 col-end-13 mr-1 text-right">
                <p className="text-5xl opacity-50 Dosis-Bold text-boxSecond">1.</p>
                <p className="self-end pt-8 text-xs Dosis-Regular">Read More...</p>
              </div>
            </div>
          </div>
        </div>
        {/* for tab */}
        <div className="hidden tab:block">
          <div className="grid grid-cols-12 mx-2 text-white gap-x-4">
            <div className="relative col-start-1 col-end-6 bg-title rounded-half-circle">
              <div className="absolute z-10 flex flex-col">
                <div className="tab:p-6 laptop:p-8">
                  <p className="pt-8 tab:text-4xl laptop:text-6xl Dosis-Medium">BUILDING INSUVAII</p>
                  <p className="Dosis-Regular">Brief two line description to create interest similar to headline!</p>
                </div>
                <div className="flex flex-row justify-between p-8">
                  <p className="w-1 opacity-50 tab:text-6xl laptop:text-8xl Dosis-Bold text-boxSecond">1.</p>
                  <p className="self-end mb-2 text-3xl Dosis-Regular">Read More...</p>
                </div>
              </div>
            </div>
            <div className="relative col-start-6 col-end-11 bg-boxFirst rounded-half-circle">
              <div className="absolute z-10 flex flex-col">
                <div className="tab:p-6 laptop:p-8">
                  <p className="pt-8 tab:text-4xl laptop:text-6xl Dosis-Medium">BUILDING INSUVAII</p>
                  <p className="Dosis-Regular">Brief two line description to create interest similar to headline!</p>
                </div>
                <div className="flex flex-row justify-between p-8">
                  <p className="w-1 opacity-50 tab:text-6xl laptop:text-8xl Dosis-Bold text-boxSecond">2.</p>
                  <p className="self-end mb-2 text-3xl Dosis-Regular">Read More...</p>
                </div>
              </div>
            </div>
            <div className="relative col-start-11 col-end-13 overflow-hidden">
              <div className="inline-block h-full tab:w-72 laptop:w-96 bg-title rounded-l-half-circle">
                <div className="table text-white tab:p-6 laptop:p-8">
                  <p className="pt-8 tab:text-4xl laptop:text-6xl Dosis-Medium">BUILDING INSUVAII</p>
                  <p className="Dosis-Regular">Brief two line description to create interest similar to headline!</p>
                </div>
                <div className="flex flex-row justify-between p-8">
                  <p className="w-1 opacity-50 tab:text-6xl laptop:text-8xl Dosis-Bold text-boxSecond">3.</p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <img
          src={Arrow}
          alt="Click backward to get new set of three case studies"
          className="w-3 transform -rotate-180 tab:w-5"
        />
      </div>
    </section>
  )
}
import Arrow from "../../svg/arrow.svg";
import { CareerForm } from "../Reusable/CareerForm";

export function Careers() {
  return (
    <section id="careers">
      <h1 className="relative z-10 hidden tab:block">CAREERS</h1>
      <h5 className="relative block tab:hidden">CAREERS</h5>
      <div className="mx-8 mt-2 mb-6 tab:my-8 tab:grid tab:grid-cols-2">
        <div className="text-center tab:col-start-1 tab:col-end-1 tab:text-left">
          {/* <p className="text-xl Dosis-SemiBold text-title">Current Openings</p>
          <div className="flex flex-col mt-4 mb-2 tab:w-5/6">
            <p className="text-xl Dosis-SemiBold text-title">VueJS Developer</p>
            <p>Junior Front End Developer who have exposure to HTML, CSS, JS. Advantage to those candidates with knowledge of ReactJS</p>
          </div>
          <div className="flex flex-col mb-2 tab:w-5/6">
            <p className="text-xl Dosis-SemiBold text-title">VueJS Developer</p>
            <p>Junior Front End Developer who have exposure to HTML, CSS, JS. Advantage to those candidates with knowledge of ReactJS</p>
          </div>
          <div className="flex flex-col mb-2 tab:w-5/6">
            <p className="text-xl Dosis-SemiBold text-title">VueJS Developer</p>
            <p>Junior Front End Developer who have exposure to HTML, CSS, JS. Advantage to those candidates with knowledge of ReactJS</p>
          </div>
          <img
            src={Arrow}
            alt="Click backward to get new set of three case studies"
            className="w-3 transform -rotate-90 tab:w-5"
          /> */}
        </div>
        <div className="z-10 tab:col-start-2 tab:col-end-2 pb-8">
          <div className="w-full h-64 mx-auto tab:w-5/6 tab:h-full">
            <div className="">
              <CareerForm />
            </div>
            <p className="ml-2 text-sm tab:text-2xl Dosis-Regular">hr@monbgul.com</p>
          </div>
        </div>
      </div>
    </section>
  )
}
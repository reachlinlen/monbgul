import { Link } from "remix";
import Logo from "../../svg/logo.svg";
import MenuBar from "../../svg/menubar.svg";
import Search from "../../svg/searchIcon.svg";

export function NavBar() {
  return (
    <header className="relative mx-auto overflow-hidden">
      <div className="grid grid-cols-12 mt-3">
        <div className="col-start-5 col-end-9 tab:col-start-1 tab:col-end-4">
          <img src={Logo} className="w-full" />
        </div>
        <div className="col-start-11 col-end-13 mt-auto tab:col-start-4 tab:mt-0">
          <div className="relative grid grid-cols-12 tab:h-10 tab:mt-3 tab:mb-2">
            <div className="hidden tab:col-start-8 tab:col-end-13 tab:block">
              <input type="text" className="w-full h-6 pl-10 tab:h-full bg-bgGrey rounded-xl" disabled />
              <img src={Search} className="w-6 ml-3 -mt-8" />
            </div>
            <div className="col-start-1 col-end-13 tab:hidden justify-self-center">
              <img src={MenuBar} className="w-10 h-6" />
            </div>
          </div>
          <nav>
            <ul className="flex-row items-end justify-between hidden pl-10 text-xl list-none desktop:text-2xl tab:flex font-DZ-Regular text-title">
              <li>
                <a href="#about">About</a>
              </li>
              <li>
                <a href="#services">Services</a>
              </li>
              <li>
                <a href="#whyus">Why Us</a>
              </li>
              <li>
                <Link to="/thirangal" target='_blank'>Thirangal</Link>
              </li>
              <li>
                <a href="#casestudy">CaseStudy</a>
              </li>
              <li>
                <a href="#blog">Blogs</a>
              </li>
              <li>
                <a href="#team">Team</a>
              </li>
              <li>
                <a href="#contact">Contact</a>
              </li>
            </ul>
          </nav>
        </div>
      </div>
    </header>
  )
}
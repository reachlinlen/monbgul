import AWS from "../../svg/aws.svg";
import Azure from "../../svg/azure.svg";

export function Partners() {
  return (
    <section id="partners" className="w-4/6 mx-auto bg-white rounded-3xl tab:rounded-circle">
      <h1 className="hidden -mt-24 tab:block">PARTNERS</h1>
      <h5 className="block -mt-12 tab:hidden">PARTNERS</h5>
      <div className="flex flex-col p-3 tab:mt-4 tab:p-8 gap-y-2">
        {/* <img src={AWS} alt="" className=""/> */}
        <div className="flex flex-row gap-x-1 tab:gap-x-2">
          <img src={AWS} alt="" className="w-1/2 p-4" />
          <img src={AWS} alt="" className="w-1/2 p-4" />
        </div>
        <div className="flex flex-row gap-x-1 tab:gap-x-2">
          <img src={Azure} alt="" className="w-1/2 p-4" />
          <img src={AWS} alt="" className="w-1/2 p-4" />
        </div>
        <div className="flex flex-row gap-x-1 tab:gap-x-2">
          <img src={AWS} alt="" className="w-1/2 p-4" />
          <img src={Azure} alt="" className="w-1/2 p-4" />
        </div>
      </div>
    </section>
  )
}
export function About() {
  return (
    <section id="about" className="mx-2 mt-2 largeMob:mx-10 tab:mx-20 laptop:mx-48 tab:mt-6 laptop:mt-8">
      <h1 className="hidden tab:block">ABOUT US</h1>
      <h5 className="block tab:hidden">ABOUT US</h5>
      <div className="text-xs text-center tab:text-xl tab:pt-2 laptop:pt-6 Dosis-Regular">
        <p>Monbgul (மங்குல்) means ‘cloud’ in Thamizh language</p>
        <p className="my-2 tab:my-4">We enable SMEs to tap Could platform while SMEs can concentrate on their core competencies
          and deliver their services reliably without building a separate IT team.</p>
        <p>With Cloud, SMEs not only have the opportunity to use technologies that were once available only to big enterprises,
          they can run the applications at low cost, fast to market and create reliable software.</p>
        <h6 className="mt-1 tab:hidden">Our Capacity</h6>
        <h2 className="hidden mt-4 tab:block">Our Capacity</h2>
        <p>Our team has more than a decade of experience in Web & Cloud technologies
          with expertise ranging from Websites, Applications to Cloud Native software.</p>
        <h6 className="mt-1 tab:hidden">Our Mission</h6>
        <h2 className="hidden mt-4 tab:block">Our Mission</h2>
        <p>To become first stop for SMEs for their web & cloud applications enabling SMEs to grow faster
          & reduce IT spending.</p>
        <h6 className="mt-1 tab:hidden">Our Values</h6>
        <h2 className="hidden mt-4 tab:block">Our Values</h2>
        <p>Honesty & Transparency</p>
      </div>
    </section>
  )
}
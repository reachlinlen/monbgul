export function ClientSpeak() {
  return (
    <section className="h-40 bg-no-repeat tab:mb-8 bg-red-box tab:h-96">
      <div className="p-2 tab:p-4">
        <h1 className="hidden mb-3 text-white tab:mt-6 tab:block">CLIENTS SPEAK</h1>
        <h5 className="block text-white tab:hidden">CLIENTS SPEAK</h5>
        <p className="mx-2 text-xs text-center text-white tab:mx-28 tab:text-2xl Dosis-Regular tab:mt-8">"Monbgul demonstrated their in-depth knowledge in technology and
          although being new, their ability to learn quickly with new
          techniques made this development journey a breeze"</p>
        <p className="pb-4 mx-16 text-xs text-center text-white tab:text-2xl Dosis-Regular">- Gavaskar, CEO, Insuvaii</p>
      </div>
    </section>
  )
}
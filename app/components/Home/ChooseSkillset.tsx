import Tick from "../../svg/tick.svg";

export function ChooseSkillset() {
  return (
    <section id="whyus" className="mt-8 mb-8 tab:mt-16 tab:mb-12">
      <div className="w-full px-8 text-center Dosis-Regular">
        {/* WHY CHOOSE US */}
        <h1 className="hidden tab:block">WHY CHOOSE US?</h1>
        <h5 className="block tab:hidden">WHY CHOOSE US?</h5>
        <div className="pt-2 pb-4 text-xs tab:py-8 tab:text-xl Dosis-Regular">
          <p>
            We are eager to bring latest technology to SME enterprises
          </p>
          <p className="pb-4 tab:pb-8">so SMEs can concentrate on their core business.</p>
          <p>With us, Explore the open source web and cloud opportunities</p>
          <p>to leverage for long-term agility and drive growth.</p>
        </div>
        {/* FOR TABLET */}
        <div className="hidden tab:block">
          <div className="flex flex-row justify-between w-11/12 mx-auto text-xl">
            <div className="flex">
              <img src={Tick} className="self-start w-8" />
              <p className="pl-1 text-left">Reliable, Scalable & Secured</p>
            </div>
            <div className="flex">
              <img src={Tick} className="self-start w-8" />
              <p className="pl-1 text-left">Low-cost, Fast to market & Reliable</p>
            </div>
            <div className="flex">
              <img src={Tick} className="self-start w-8" />
              <p className="pl-1 text-left">Reliable, Scalable & Secured</p>
            </div>
            <div className="flex">
              <img src={Tick} className="self-start w-8" />
              <p className="pl-1 text-left">Reliable, Scalable & Secured</p>
            </div>
          </div>
        </div>
        {/* FOR MOBILE */}
        <div className="block tab:hidden">
          <div className="grid grid-cols-2 justify-items-center">
            <div className="w-3/4 col-start-1 col-end-2">
              <div className="flex">
                <img src={Tick} className="self-start w-4" />
                <p className="pl-1 text-xs text-left">Reliable, Scalable & Secured</p>
              </div>
              <div className="flex mt-4">
                <img src={Tick} className="self-start w-4" />
                <p className="pl-1 text-xs text-left">Low-cost, Fast to market & Reliable</p>
              </div>
            </div>
            <div className="w-3/4 col-start-2 col-end-3">
              <div className="flex">
                <img src={Tick} className="self-start w-4" />
                <p className="pl-1 text-xs text-left">Reliable, Scalable & Secured</p>
              </div>
              <div className="flex mt-4">
                <img src={Tick} className="self-start w-4" />
                <p className="pl-1 text-xs text-left">Low-cost, Fast to market & Reliable</p>
              </div>
            </div>
          </div>
        </div>
        {/* SKILL SET */}
        {/* FOR TABLET */}
        <div className="hidden my-8 tab:block">
          <h1 className="mb-4">SKILL SET</h1>
          <div className="relative flex flex-row justify-between w-11/12 mx-auto text-xl Dosis-Regular">
            <p>Efficient Project Management</p>
            <div className="border-l-2 border-gray-500" />
            <p>Expertise in Cloud Technologies</p>
            <div className="border-l-2 border-gray-500" />
            <p>Core Foundation on Web Application</p>
            <div className="border-l-2 border-gray-500" />
            <p>Smart Problem Solving</p>
          </div>
        </div>
        {/* FOR MOBILE */}
        <div className="block my-4 text-xs tab:hidden">
          <h5>SKILL SET</h5>
          <div className="relative grid grid-cols-12 justify-items-center">
            <div className="w-2/3 col-start-1 col-end-6">
              <p>Efficient Project Management</p>
              <p className="pt-4">Smart Problem Solving</p>
            </div>
            <div className="self-center col-start-6 col-end-7">
              <div className="h-12 border-l-2 border-gray-500" />
            </div>
            <div className="w-2/3 col-start-7 col-end-13">
              <p>Core Foundation on Web Application</p>
              <p className="pt-4">Expertise in Cloud Technologies</p>
            </div>
          </div>
        </div>
      </div>
    </section>
  )
}
import BlogIllustration from "../../svg/blogIllustration.svg";
// import DotsRectangleArt from "../../svg/dotsRectangleArt.svg";

export function Blogs() {
  return (
    <section id="blog" className="flex flex-col w-11/12 py-4 ml-auto text-white transform -translate-y-16 tab:py-8 rounded-l-circle bg-title">
      <h1 className="hidden mb-6 text-center text-white tab:block">BLOGS</h1>
      <h5 className="block mb-3 text-center text-white tab:hidden">BLOGS</h5>
      <p className="w-3/5 ml-8 text-xl tab:ml-12 tab:text-5xl Dosis-Medium">Is Cloud Computing only for Big Enterprises?</p>
      <p className="w-4/6 mt-4 ml-8 text-xs tab:text-base tab:ml-12 Dosis-Medium">Cloud computing namely AWS, Azure are the buzz words in IT world. But is Cloud only for large enterprises?
        It's a myth. Cloud computing can be leveaaged by SMEs to efficiently run IT operations.
      </p>
      <img src={BlogIllustration} className="self-end w-1/3 mr-4 -mt-6 tab:-mt-12" />
      <p className="self-start pb-2 ml-8 text-base tab:ml-12 tab:text-2xl Dosis-Regular tab:pb-0">Read More...</p>
      {/* <img src={DotsRectangleArt} className="absolute origin-bottom-right transform -rotate-90 w-24 mt-16 ml-40" /> */}
    </section>
  )
}
import ResponsiveIllustration from "../../svg/responsiveIllustration.svg";
import NativeCloud from "../../svg/nativeCloudIllustration.svg";
import CloudMigration from "../../svg/cloudMigrationIllustration.svg";
import BgCardDarkRed from "../../svg/bgCardDarkRed.svg";
import BgCardLightRed from "../../svg/bgCardLightRed.svg";
import BgCardBlackRedSquare from "../../svg/bgCardBlackRedSquare.svg";

export function Services() {
  return (
    <section id="services" className="mt-4 tab:mt-10">
      {/* For Tab / laptop */}
      <div className="hidden tab:block">
        <h1>SERVICES</h1>
        <div className="grid grid-cols-2 text-white">
          <div className="flex flex-col col-start-1 col-end-2">
            <div className="flex flex-col p-8 bg-no-repeat bg-card-dark-red">
              <img src={ResponsiveIllustration} alt="responsive web design illustration" className="self-end w-2/6" />
              <p className="text-7xl Dosis-SemiBold">WEB</p>
              <p className="pb-8 text-6xl Dosis-Medium">APPLICATION</p>
              <div className="text-base Dosis-Regular">
                <p>Legacy Code Migration</p>
                <p>Design, Software Engineering</p>
                <p>Design System</p>
                <p>FrontEnd System (ReactJS, VueJS)</p>
                <p>BackEnd System (JAVA, Spring Boot, NodeJS, DenoJS)</p>
              </div>
              <p className="self-end text-base mt-36 Dosis-Regular">Know More...</p>
            </div>
            <div className="flex flex-col pt-10 pl-8 pr-16 bg-no-repeat pb-14 bg-card-black-red-rectangle">
              <div className="text-6xl Dosis-SemiBold">
                <p>ANYTHING</p>
                <p>ELSE?</p>
              </div>
              <button className="self-end px-4 py-2 mt-auto text-2xl bg-white text-boxSecond rounded-3xl Dosis-Bold">
                Call Us
              </button>
            </div>
          </div>
          <div className="z-10 flex flex-col col-start-2 col-end-3">
            <div className="flex flex-col p-8 pb-6 bg-no-repeat bg-card-black-red-square">
              <img src={NativeCloud} className="self-end w-1/3" />
              <p className="text-7xl Dosis-SemiBold">NATIVE</p>
              <p className="pb-4 text-6xl Dosis-Medium">CLOUD</p>
              <div className="text-base Dosis-Regular">
                <p>Legacy Code Migration</p>
                <p>Design, Software Engineering</p>
              </div>
              <p className="self-end Dosis-Regular"> Know More...</p>
            </div>
            <div className="flex flex-col p-8 pb-12 bg-no-repeat bg-card-light-red">
              <img src={CloudMigration} className="self-end w-1/4" />
              <p className="text-7xl Dosis-SemiBold">CLOUD</p>
              <p className="pb-8 text-6xl Dosis-Medium">MIGRATION</p>
              <div className="text-base Dosis-Regular">
                <p>Application Migration</p>
                <p>Data Migration</p>
                <p>On-premise infrastructure to Cloud</p>
              </div>
              <p className="self-end text-base mt-14 Dosis-Regular">Know More...</p>
            </div>
          </div>
        </div>
      </div>
      {/* For Mobile */}
      <div className="block tab:hidden">
        <h5>SERVICES</h5>
        <div className="mx-4 text-white">
          <div className="relative grid">
            <div className="col-start-1 col-end-2 row-start-1 row-end-2">
              <img src={BgCardDarkRed} className="" />
            </div>
            <div className="flex flex-col items-end col-start-1 col-end-2 row-start-1 row-end-2 px-4 pt-8">
              <img src={ResponsiveIllustration} alt="responsive web design illustration" className="w-1/3" />
              <p className="mt-6 mr-8 text-3xl Dosis-SemiBold">WEB APPLICATION</p>
              <div className="my-6 mr-6 text-base Dosis-Regular">
                <p>Legacy Code Migration</p>
                <p className="my-1">Design, Software Engineering</p>
                <p className="my-1">Design System</p>
                <p className="my-1">FrontEnd System (ReactJS, VueJS)</p>
                <p className="my-1">BackEnd System (JAVA, Spring Boot, NodeJS, DenoJS)</p>
              </div>
              <p className="self-end justify-end text-xs Dosis-Regular">Know More...</p>
            </div>
          </div>
          <div className="flex flex-col items-start p-4 bg-no-repeat bg-card-black-red-square">
            <img src={NativeCloud} className="self-end w-1/3" />
            <p className="mt-6 ml-12 text-3xl Dosis-SemiBold">NATIVE CLOUD</p>
            <div className="mt-3 text-base Dosis-Regular">
              <p>Legacy Code Migration</p>
              <p>Design, Software Engineering</p>
            </div>
            <p className="self-end text-xs Dosis-Regular"> Know More...</p>
          </div>
          <div className="relative grid">
            <div className="col-start-1 col-end-2 row-start-1 row-end-2">
              <img src={BgCardLightRed} className="" />
            </div>
            <div className="flex flex-col items-start col-start-1 col-end-2 row-start-1 row-end-2 px-4 pt-8">
              <img src={CloudMigration} className="self-end w-1/3" />
              <p className="mt-6 mr-8 text-3xl Dosis-SemiBold">CLOUD MIGRATION</p>
              <div className="my-6 mr-6 text-base Dosis-Regular">
                <p>Application Migration</p>
                <p>Data Migration</p>
              </div>
              <p className="self-end justify-end text-xs Dosis-Regular">Know More...</p>
            </div>
          </div>
          <div className="relative grid">
            <div className="col-start-1 col-end-2 row-start-1 row-end-2">
              <img src={BgCardBlackRedSquare} className="" />
            </div>
            <div className="flex flex-col items-start col-start-1 col-end-2 row-start-1 row-end-2 px-4 pt-8">
              <p className="mt-6 mr-8 text-3xl Dosis-SemiBold">ANYTHING ELSE?</p>
              <button className="self-end px-4 mt-auto mb-4 text-xl bg-white text-boxSecond rounded-3xl Dosis-Bold">
                Call Us
              </button>
            </div>
          </div>
        </div>
      </div>
    </section>
  )
}
import Arrow from "../../svg/arrow.svg";
import LinkedIn from "../../svg/linkedin.svg";
import Polywork from "../../svg/polywork.svg";
import Twitter from "../../svg/twitter.svg";
import memberOneImg from "../../images/Lenin.jpg";
import { Link } from "remix";

export function Team() {
  return (
    <section id="team" className="my-4 tab:my-16">
      {/* TODO: Once I quit the job */}
      {/* <h1>OUR TEAM</h1> */}
      <h1 className="hidden tab:block">OUR MENTORS</h1>
      <h5 className="block tab:hidden">OUR MENTORS</h5>
      <div className="flex flex-row mx-2 mt-4 tab:mt-16">
        {/* <img
          src={Arrow}
          alt="Click backward to see team members"
          className="w-3 tab:w-5"
          loading="lazy"
        /> */}
        <div className="z-10 tab:grid tab:grid-cols-3 pl-4">
          {/* First team member */}
          <div className="tab:col-start-1 tab:col-end-1">
            <div className="flex flex-col items-center gap-y-2">
              <div className="bg-top bg-no-repeat bg-contain w-44 h-44 rounded-circle" style={{ backgroundImage: `url(${memberOneImg})` }} />
              <p className="text-2xl Dosis-SemiBold text-title">Lenin M.</p>
              <div className="flex flex-row gap-x-4">
                <a href="https://www.linkedin.com/in/lealen/" target="_blank">
                  <img src={LinkedIn} className="w-6" loading="lazy" />
                </a>
                <a href="https://www.polywork.com/_len" target="_blank">
                  <img src={Polywork} className="w-6" loading="lazy" />
                </a>
                <a href="https://twitter.com/ungalLenin" target="_blank">
                  <img src={Twitter} className="w-6" loading="lazy" />
                </a>
              </div>
              <p className="ml-4 mr-2 line-clamp-4">Web Application Developer. Very passionate about technology & its application.</p>
            </div>
          </div>
          {/* Second team member */}
          {/* <div className="hidden col-start-2 col-end-2 tab:block">
            <div className="flex flex-col items-center gap-y-2">
              <div className="bg-top bg-no-repeat bg-contain w-44 h-44 rounded-circle" style={{ backgroundImage: `url(${memberOneImg})` }} />
              <p className="text-2xl Dosis-SemiBold text-title">Lenin M.</p>
              <div className="flex flex-row gap-x-4">
                <img src={LinkedIn} className="w-6" />
                <img src={Polywork} className="w-6" />
                <img src={Twitter} className="w-6" />
              </div>
              <p className="ml-4 mr-2 line-clamp-4">Avid programmer and sports person.
                Very passionate about technology and its application</p>
            </div>
          </div> */}
          {/* Third team member */}
          {/* <div className="hidden col-start-3 col-end-3 tab:block">
            <div className="flex flex-col items-center gap-y-2">
              <div className="bg-top bg-no-repeat bg-contain w-44 h-44 rounded-circle" style={{ backgroundImage: `url(${memberOneImg})` }} />
              <p className="text-2xl Dosis-SemiBold text-title">Lenin M.</p>
              <div className="flex flex-row gap-x-4">
                <img src={LinkedIn} className="w-6" />
                <img src={Polywork} className="w-6" />
                <img src={Twitter} className="w-6" />
              </div>
              <p className="ml-4 mr-2 line-clamp-4">Avid programmer and sports person.
                Very passionate about technology and its application</p>
            </div>
          </div> */}
        </div>
        {/* <img
          src={Arrow}
          alt="Click forwatd to see more team members"
          className="w-3 transform -rotate-180 tab:w-5"
        /> */}
      </div>
    </section>
  )
}
import { Link, Outlet } from "remix";
import Logo from "../svg/logo.svg";

export default function Thirangal() {
  return (
    <>
      <header className="relative mx-auto overflow-hidden px-4">
        <nav className="grid grid-cols-12 mt-3">
          <div className="col-start-5 col-end-9 tab:col-start-1 tab:col-end-4">
            <Link to="/thirangal">
              <img src={Logo} className="w-full" />
            </Link>
          </div>
          <div className="col-start-5 col-end-13 mt-auto tab:col-start-5 tab:mt-0 flex flex-col items-end">
            <a href="#contact">
              <button className="p-1 my-1 text-xs border-title border bg-gradient-light-dark tab:px-3 tab:text-xl rounded-2xl w-full Dosis-SemiBold bg-title text-white hover:text-title hover:background-white">
                Contact Us
              </button>
            </a>
            <ul className="flex-row items-end justify-between hidden pl-10 text-xl list-none desktop:text-2xl tab:flex h-1/2 font-DZ-Regular text-title">
              <li className="nav-bar-item">
                <Link to="/thirangal/experience">Experience</Link>
              </li>
              <li className="nav-bar-item">
                <Link to="/thirangal/expertise">Expertise</Link>
              </li>
              <li className="nav-bar-item">
                <Link to="/thirangal/experiments">Experiments</Link>
              </li>
            </ul>
          </div>
        </nav>
      </header>
      <main>
        <Outlet />
      </main>
    </>
  )
}
import { Outlet } from "remix";
import BannerLayout from "~/components/Reusable/BannerLayout";
import ExpertiseBanner from "~/svg/expertiseBanner.svg";

const BANNER_TITLE = "Cutting Edge Technology!";

const BANNER_INFO = `Our team has extensive experinece in developing 
Web & Cloud applications in various sectors ranging from Banking / Financial 
institutions to Logistics sector.`;

export default function Experience() {
  return (
    <>
      <BannerLayout
        banner={ExpertiseBanner}
        bannerTitle={BANNER_TITLE}
        bannerInfo={BANNER_INFO}
      />
      <main>
        <Outlet />
      </main>
    </>
  )
}
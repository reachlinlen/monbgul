import { MetaFunction, useLoaderData } from "remix";
import { ThirangalLayout } from "~/components/Thirangal/ThirangalMainLayout";
import { getFiles } from "~/utils/github.server";

export let meta: MetaFunction = () => {
  return {
    title: "Experiments",
    description: "Exploring new techniques & new technologies!"
  };
};

export type Post = {
  topic: string,
  URL: string
}

const PLACEHOLDER_TEXT = "Search for White Papers...";

const POSTS_LIST = [
  {
    topic: "How SMEs can use Cloud Technologies?",
    description: "Best practices always bring best products & solutions within the stipulated time.",
    URL: "/thirangal/experiment/sme-cloud.tsx"
  },
  {
    topic: "How SMEs can use Cloud Technologies?",
    description: "Best practices always bring best products & solutions within the stipulated time.",
    URL: "/thirangal/experiment/sme-cloud.tsx"
  },
  {
    topic: "Building Insuvaii",
    description: "Best practices always bring best products & solutions within the stipulated time.",
    URL: "/thirangal/experiment/sme-cloud.tsx"
  },
  {
    topic: "How SMEs can use Cloud Technologies?",
    description: "Best practices always bring best products & solutions within the stipulated time.",
    URL: "/thirangal/experiment/sme-cloud.tsx"
  },
  {
    topic: "How SMEs can use Cloud Technologies?",
    description: "Best practices always bring best products & solutions within the stipulated time.",
    URL: "/thirangal/experiment/sme-cloud.tsx"
  },
]

export const loader = () => {
  return getFiles('contents/blog')
}

export default function ExperimentsIndex() {
  const posts = useLoaderData();
  console.log({ posts })
  return (
    <ThirangalLayout
      postsList={posts}
      numberPostsPerLine={2}
      placeholderText={PLACEHOLDER_TEXT}
    />
  )
}
import { MetaFunction } from "@remix-run/server-runtime";
import ThirangalBannerBg from "~/svg/thirangalBannerBg.svg";

export let meta: MetaFunction = () => {
  return {
    title: "Thirangal",
    description: "Thirangal of Monbgul!"
  };
};

const THIRANGAL = `Our THIRANGAL (thamizh: திறன்கள்) i.e. Competencies comprises of
          Technology Consulting and Software Development by adopting latest technology stack esp. OpenSource, modern architecure & industry standard 
          Project Management practices incl. Agile`;

export default function ThirangalIndex() {
  return (
    <>
      <div className="grid mt-5 tab:mt-6 text-center">
        <img src={ThirangalBannerBg} className="" />
        <div className="absolute w-full">
          <p className="Dosis-Bold text-white text-6xl text-center mt-32">Let's Cloud It!!!</p>
          <p className="Dosis-Regular text-3xl text-white max-w-lg mx-auto mt-10">Monbgul supports & solves SME's technological challenge in Web & Cloud architecture,
            software development & digital transformation
          </p>
        </div>
      </div>
      <div className="bg-no-repeat bg-full-nwk-bg bg-right">
        {/* <div className="mt-2 w-1/2 mx-auto">
          <SearchInput tagList={TAGLIST} />
        </div> */}
        <p className="Dosis-SemiBold text-2xl max-w-3xl mx-auto text-title text-center my-8">{THIRANGAL}</p>
        <div className="grid grid-cols-12 ml-10 gap-y-12">
          <div className="col-start-1 col-end-6">
            <div className="grid grid-cols-5">
              <div className="list-gradient">
                <div className="list-number">
                  1.
                </div>
              </div>
              <div className="list-text">
                SME Digitalisation & Sustainability
              </div>
            </div>
            <div className="grid grid-cols-12 mt-2">
              <div className="col-start-1 col-end-1 w-1/2 h-1.5 rounded-sm bg-lightRedGradientEnd mt-4" />
              <div className="col-start-2 col-end-13 Dosis-Regular text-left text-2xl">
                Understanding and analyzing the issue
              </div>
              <div className="col-start-1 col-end-1 w-1/2 h-1.5 rounded-sm bg-lightRedGradientEnd mt-4" />
              <div className="col-start-2 col-end-13 Dosis-Regular text-left text-2xl">
                Suggesting sector-specific and function-specific solutions
              </div>
            </div>
          </div>
          <div className="col-start-7 col-end-12">
            <div className="grid grid-cols-5">
              <div className="list-gradient">
                <div className="list-number">
                  2.
                </div>
              </div>
              <div className="list-text">
                SME Digital Design & Digital Security
              </div>
            </div>
            <div className="grid grid-cols-12 mt-2">
              <div className="col-start-1 col-end-1 w-1/2 h-1.5 rounded-sm bg-lightRedGradientEnd mt-4" />
              <div className="col-start-2 col-end-13 Dosis-Regular text-left text-2xl">
                Design of software application plays an important role in
                Storage, Handling, Cost and Security of both Data & App
              </div>
            </div>
          </div>
          <div className="col-start-1 col-end-6">
            <div className="grid grid-cols-5">
              <div className="list-gradient">
                <div className="list-number">
                  3.
                </div>
              </div>
              <div className="list-text">
                SME Data Management & Data Protection
              </div>
            </div>
            <div className="grid grid-cols-12 mt-2">
              <div className="col-start-1 col-end-1 w-1/2 h-1.5 rounded-sm bg-lightRedGradientEnd mt-4" />
              <div className="col-start-2 col-end-13 Dosis-Regular text-left text-2xl">
                Cloud services have high levels of Digital Security
              </div>
              <div className="col-start-1 col-end-1 w-1/2 h-1.5 rounded-sm bg-lightRedGradientEnd mt-4" />
              <div className="col-start-2 col-end-13 Dosis-Regular text-left text-2xl">
                Monbgul helps SMEs prepare Digital Security Strategy
              </div>
            </div>
          </div>
          <div className="col-start-7 col-end-12">
            <div className="grid grid-cols-5">
              <div className="list-gradient">
                <div className="list-number">
                  4.
                </div>
              </div>
              <div className="list-text">
                SME with Web & Cloud Technologies
              </div>
            </div>
            <div className="grid grid-cols-12 mt-2">
              <div className="col-start-1 col-end-1 w-1/2 h-1.5 rounded-sm bg-lightRedGradientEnd mt-4" />
              <div className="col-start-2 col-end-13 Dosis-Regular text-left text-2xl">
                Digital Technology Ecosystem is continuously evolving.
                Cloud Strategy is the best & only one way for SMEs to adopt latest technologies.
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}
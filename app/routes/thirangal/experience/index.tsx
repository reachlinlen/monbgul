import { MetaFunction, useLoaderData } from "remix";
import { ThirangalLayout } from "~/components/Thirangal/ThirangalMainLayout";

export let meta: MetaFunction = () => {
  return {
    title: "Experience",
    description: "Years of Experience!"
  };
};

const PLACEHOLDER_TEXT = "Explore our past experiences...."

const POSTS_LIST = [
  {
    topic: "How SMEs can use Cloud Technologies?",
    description: "Best practices always bring best products & solutions within the stipulated time.",
    URL: "/thirangal/experiment/sme-cloud.tsx"
  },
  {
    topic: "How SMEs can use Cloud Technologies?",
    description: "Best practices always bring best products & solutions within the stipulated time.",
    URL: "/thirangal/experiment/sme-cloud.tsx"
  },
  {
    topic: "Building Insuvaii",
    description: "Best practices always bring best products & solutions within the stipulated time.",
    URL: "/thirangal/experiment/sme-cloud.tsx"
  },
  {
    topic: "How SMEs can use Cloud Technologies?",
    description: "Best practices always bring best products & solutions within the stipulated time.",
    URL: "/thirangal/experiment/sme-cloud.tsx"
  },
  {
    topic: "How SMEs can use Cloud Technologies?",
    description: "Best practices always bring best products & solutions within the stipulated time.",
    URL: "/thirangal/experiment/sme-cloud.tsx"
  },
]

export const loader = () => {
  const tags = [
    'react', 'nodejs', 'aws', 'azure', 'cludfare'
  ];
  return tags;
}

export default function ExperienceIndex() {
  const tags = useLoaderData();
  return (
    <ThirangalLayout
      postsList={POSTS_LIST}
      numberPostsPerLine={3}
      placeholderText={PLACEHOLDER_TEXT}
      tags={tags}
    />
  )
}
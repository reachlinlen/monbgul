import { Outlet } from "remix";
import BannerLayout from "~/components/Reusable/BannerLayout";
import ExperienceBanner from "~/svg/experienceBanner.svg";

const BANNER_TITLE = "Years of Experience";

const BANNER_INFO = `Our team has extensive experinece in developing 
Web & Cloud applications in various sectors ranging from Banking / Financial 
institutions to Logistics sector.`;

export default function Experience() {
  return (
    <>
      <BannerLayout
        banner={ExperienceBanner}
        bannerTitle={BANNER_TITLE}
        bannerInfo={BANNER_INFO}
      />
      <main>
        <Outlet />
      </main>
    </>
  )
}
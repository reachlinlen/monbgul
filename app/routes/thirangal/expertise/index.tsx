import type { MetaFunction } from "remix";
import { ThirangalLayout } from "~/components/Thirangal/ThirangalMainLayout";

export let meta: MetaFunction = () => {
  return {
    title: "Expertise",
    description: "Cutting Edge Technology!"
  };
};

const PLACEHOLDER_TEXT = "Find out our expertises...";

const POSTS_LIST = [
  {
    topic: "How SMEs can use Cloud Technologies?",
    description: "Best practices always bring best products & solutions within the stipulated time.",
    URL: "/thirangal/experiment/sme-cloud.tsx"
  },
  {
    topic: "How SMEs can use Cloud Technologies?",
    description: "Best practices always bring best products & solutions within the stipulated time.",
    URL: "/thirangal/experiment/sme-cloud.tsx"
  },
  {
    topic: "Building Insuvaii",
    description: "Best practices always bring best products & solutions within the stipulated time.",
    URL: "/thirangal/experiment/sme-cloud.tsx"
  },
  {
    topic: "How SMEs can use Cloud Technologies?",
    description: "Best practices always bring best products & solutions within the stipulated time.",
    URL: "/thirangal/experiment/sme-cloud.tsx"
  },
  {
    topic: "How SMEs can use Cloud Technologies?",
    description: "Best practices always bring best products & solutions within the stipulated time.",
    URL: "/thirangal/experiment/sme-cloud.tsx"
  },
];

const SPECIALITY_DETAILS = [
  {
    count: 11, type: 'AWS Cloud Certifications'
  },
  {
    count: 11, type: 'Azure Cloud Certifications'
  },
  {
    count: 3, type: 'PMP Certifications'
  }
]

export default function ExpertiseIndex() {
  return (
    <ThirangalLayout
      postsList={POSTS_LIST}
      numberPostsPerLine={2}
      placeholderText={PLACEHOLDER_TEXT}
      specialityDetails={SPECIALITY_DETAILS}
    />
  )
}
import type { MetaFunction, LoaderFunction } from "remix";
import { useLoaderData, json, Link } from "remix";
import { About } from "~/components/Home/About";
import { Banner } from "~/components/Home/Banner";
import { Blogs } from "~/components/Home/Blogs";
import { Careers } from "~/components/Home/Careers";
import { ChooseSkillset } from "~/components/Home/ChooseSkillset";
import { ClientSpeak } from "~/components/Home/ClientSpeak";
import { NavBar } from "~/components/Home/NavBar";
import { Services } from "~/components/Home/Services";
import CircleArt from "../svg/doubleCircleDotArt.svg";
import LineArt from "../svg/fiveLines.svg";
import DotsRectangleArt from "../svg/dotsRectangleArt.svg";
import WhiteDotsCircleArt from "../svg/whiteDotsCircleArt.svg";
import HockeyStickArt from "../svg/hockeyStickArt.svg";
import RedDotsCircleArt from "../svg/redDotsCircleArt.svg";
import { Partners } from "~/components/Home/Partners";
import { CaseStudy } from "~/components/Home/CaseStudy";
import { Team } from "~/components/Home/Team";

type IndexData = {
  resources: Array<{ name: string; url: string }>;
  demos: Array<{ name: string; to: string }>;
};

// Loaders provide data to components and are only ever called on the server, so
// you can connect to a database or run any server side code you want right next
// to the component that renders it.
// https://remix.run/api/conventions#loader
export let loader: LoaderFunction = () => {
  let data: IndexData = {
    resources: [
      {
        name: "Remix Docs",
        url: "https://remix.run/docs"
      },
      {
        name: "React Router Docs",
        url: "https://reactrouter.com/docs"
      },
      {
        name: "Remix Discord",
        url: "https://discord.gg/VBePs6d"
      }
    ],
    demos: [
      {
        to: "demos/actions",
        name: "Actions"
      },
      {
        to: "demos/about",
        name: "Nested Routes, CSS loading/unloading"
      },
      {
        to: "demos/params",
        name: "URL Params and Error Boundaries"
      }
    ]
  };

  // https://remix.run/api/remix#json
  return json(data);
};

// https://remix.run/api/conventions#meta
export let meta: MetaFunction = () => {
  return {
    title: "Monbgul",
    description: "Welcome to Monbgul!"
  };
};

// https://remix.run/guides/routing#index-routes
export default function Index() {
  let data = useLoaderData<IndexData>();

  return (
    <>
      <div className="px-4 tab:px-8">
        <NavBar />
      </div>
      <div className="px-2 tab:px-4">
        <Banner />
        <div className="absolute grid w-full grid-cols-6 largeMob:grid-cols-4">
          <div className="self-end col-start-1 col-end-4 largeMob:col-end-3 laptop:self-center">
            <img src={CircleArt} className="w-4/5 transform -translate-y-20 tab:w-2/3 laptop:translate-y-0" />
          </div>
          <div className="col-start-5 col-end-7 largeMob:col-start-4 largeMob:col-end-5">
            <img src={LineArt} className="w-5/6 ml-auto transform -translate-y-20 largeMob:ml-0 largeMob:w-full largeMob:-translate-y-24 tab:-translate-y-40 laptop:-translate-y-60" />
          </div>
        </div>
        <About />
        <Services />
        <img src={RedDotsCircleArt} className="absolute w-2/3 transform -translate-y-20 -translate-x-36 tab:-translate-x-72" />
        <ChooseSkillset />
        {/* TODO: Add along with partners */}
        <img src={DotsRectangleArt} className="absolute hidden tab:block tab:w-28 laptop:w-32 mt-36 right-6" />
        {/* TODO: ADD AFTER GETTING A CLIENT */}
        <ClientSpeak />
        <div className="py-6 bg-bgGrey tab:rounded-circle rounded-half-circle">
          {/* TODO: ADD AFTER BECOMING PARTNER */}
          <Partners />
          <CaseStudy />
          <img src={HockeyStickArt} className="absolute hidden w-1/3 laptop:ml-36 laptop:-mt-96 tab:block tab:ml-32 tab:-mt-64" />
          <Team />
          <img src={WhiteDotsCircleArt} className="absolute w-2/3 transform translate-x-96 -translate-y-80" />
          <Careers />
        </div>
        <Blogs />
      </div>
    </>
  );
}

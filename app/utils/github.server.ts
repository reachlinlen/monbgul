import nodePath from 'path'
import { bundleMDX } from 'mdx-bundler'
// const PRIVATE_TOKEN = 'glpat-D3RttdgyzWMcHQdR9c39'

const BASE_URL = 'https://gitlab.com/api/v4/projects/31983831/repository/'

type Post = {
  id: string,
  name: string,
  type: string,
  path: string,
  mode: string
}
async function getFiles(path: string) {
  const resp: Post[] = await fetch(`${BASE_URL}tree?path=${path}`).then(res => res.json())
  const posts = resp.filter(post => post.type == 'blob')
  const res = await Promise.all(
    posts
      .map(async function getPostFiles(post) {
        const fileDetails = await fetch(`${BASE_URL}files/${encodeURIComponent(post.path)}?ref=main`, { method: 'GET' })
          .then(res => res.json())
        return await bundleMDX({
          source: fileDetails.content,
          files: {}
        })
      })
  )
  return res
}

export {
  getFiles
}

module.exports = {
  mode: "jit",
  content: ["./app/**/*.{ts,tsx,js,jsx}"],
  darkMode: "media", // or 'media' or 'class'
  theme: {
    screens: {
      'largeMob': '414px',
      'tab': '768px',
      'laptop': '1024px',
      'desktop': '1280px'
    },
    colors: {
      title: '#ce3939',
      body: '#170b0c',
      bgGrey: '#dfdfdf',
      boxFirst: '#d34f32',
      boxSecond: '#90211d',
      lightRedGradientEnd: '#ef4206',
      darkRedGradientEnd: '#99211f',
      contact: '#777777',
      white: '#fff',
    },
    extend: {
      fontFamily: {
        'DZ-Regular': ['DuepuntozeroRegular', 'sans'],
        'Dosis': ['Dosis', 'sans'],
      },
      borderRadius: {
        'circle': '88px',
        'half-circle': '48px',
        'services': '20px',
      },
    },
    backgroundImage: theme => ({
      'banner-pattern': "url('/bgsvg/bgBanner.svg')",
      'card-dark-red': "url('/bgsvg/bgCardDarkRed.svg')",
      'card-light-red': "url('/bgsvg/bgCardLightRed.svg')",
      'card-black-red-square': "url('/bgsvg/bgCardBlackRedSquare.svg')",
      'card-black-red-rectangle': "url('/bgsvg/bgCardBlackRedRectangle.svg')",
      'web-application': "url('/bgsvg/webApplication.svg')",
      'red-dots-circle-art': "url('/bgsvg/redDotsCircleArt.svg')",
      'red-box': "url('/bgsvg/redBox.svg')",
      'red-tile': "url('/bgsvg/redTileBg.svg')",
      'red-tile-no-asp-ratio': "url('/bgsvg/redTileBgNoAspRation.svg')",
      'orange-tile': "url('/bgsvg/orangeTileBg.svg')",
      'thirangal-banner-bg': "url(/bgsvg/thirangalBannerBg.svg)",
      'side-nwk-bg': "url(/bgsvg/sideNetwork.svg)",
      'full-nwk-bg': "url(/bgsvg/fullNetwork.svg)",
      'lenin': "url('/images/Lenin.jpg')",
    }),
  },
  variants: {},
  plugins: [
    require('@tailwindcss/aspect-ratio'),
    require('@tailwindcss/line-clamp'),
  ]
};